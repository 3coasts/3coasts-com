import * as handlers from './handlers'

export default [
  { method: 'get', path: '/', handler: handlers.home },
  { method: 'post', path: '/contact', handler: handlers.contact },
  { method: 'get', path: '/projects/{id}', handler: handlers.project },
  { method: 'get', path: '/admin/projects/{id}', handler: handlers.project },

  { method: 'get', path: '/api/auth', handler: handlers.auth },
  { method: 'delete', path: '/api/auth', handler: handlers.clearAuth },
  { method: 'post', path: '/api/images', handler: handlers.images.create },
  { method: 'get', path: '/api/images/{id}', handler: handlers.images.findOne },
  { method: 'get', path: '/api/projects', handler: handlers.projects.list },
  { method: 'post', path: '/api/projects', handler: handlers.projects.save },
  { method: 'get', path: '/api/projects/{id}', handler: handlers.projects.findOne },
  { method: 'delete', path: '/api/projects/{id}', handler: handlers.projects.remove },
  { method: 'get', path: '/api/youtube/{id}', handler: handlers.youtube }
]
