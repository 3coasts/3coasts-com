import Joi from 'joi'

export const parse = (obj) => {
  return {
    ...obj,
    body: JSON.parse(obj.body || '[]'),
    image: JSON.parse(obj.image || '{}')
  }
}

export const schema = {
  id: Joi.string().uuid().allow(null),
  title: Joi.string(),
  teaser: Joi.string(),
  body: Joi.array().allow(null),
  image: Joi.object({
    id: Joi.string().uuid(),
    extension: Joi.string().max(100),
    type: Joi.string().max(100),
    width: Joi.number(),
    height: Joi.number(),
    gravity: Joi.string().allow(null)
  }),
  createdAt: Joi.number(),
  updatedAt: Joi.number()
}
