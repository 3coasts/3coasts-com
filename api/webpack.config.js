const webpack = require('webpack')

module.exports = {
  target: 'node',
  entry: './lib/api.js',
  output: {
    filename: 'index.js',
    path: __dirname,
    library: 'library',
    libraryTarget: 'umd'
  },
  // externals: {
  //   'styled-components/lib/models/StyleSheet': 'commonjs styled-components/src/models/StyleSheet'
  // },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    })
  ],
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      use: ['babel-loader']
    }, {
      test: /\.(css)$/,
      exclude: /node_modules/,
      use: ['raw-loader']
    }]
  }
}
