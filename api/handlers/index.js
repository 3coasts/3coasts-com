import { select, get } from '../lib/simpledb'
import * as projects from './projects'
import * as images from './images'
import config from '../../config'
import axios from 'axios'
import { stringify as qs } from 'querystring'
import email from '../lib/email'
import ContactForm from '../lib/email/templates/contact-form'
export { projects, images }

const gm = require('gm').subClass({imageMagick: true})

export const home = () =>
  select('Project', `SELECT * FROM Project`)
    .then(projects => ({ data: { projects } }))

export const project = (req) =>
  get('Project', req.pathParams.id)
    .then(project => ({ data: { projects: [project] } }))

export const auth = (req, auth = {}) => Promise.resolve({
  data: auth,
  statusCode: 200,
  headers: {
    'Set-Cookie': `Authorization=${auth.authorization}; Path=/`,
    'Content-Type': 'application/json'
  },
})

export const clearAuth = (req, auth = {}) => Promise.resolve({
  statusCode: 200,
  data: auth,
  headers: { 'Set-Cookie': `Authorization=; Path=/`, 'Content-Type': 'application/json' }
})

export const contact = req => axios({
  method: 'POST',
  json: true,
  url: `https://www.google.com/recaptcha/api/siteverify?${qs({
    secret: config.captcha.secretKey,
    response: req.body.verified
  })}`
}).then(() => {
  const args = {
    to: 'bri@nberl.in',
    subject: 'Message from Website',
    ...req.body
  }
  return email(ContactForm, args)
})

export const youtube = req => axios({
  url: `https://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=${req.pathParams.id}`,
  method: 'GET'
}).then(({ data }) => {
  return new Promise((resolve, reject) => {
    gm(data.thumbnail_url)
      .trim()
      .toBuffer((err, resized) => {
        if (err) reject(err)
        gm(resized).size((err, data) => {
          if (err) reject(err)
          resolve({ data })
        })
      })
  })
})
