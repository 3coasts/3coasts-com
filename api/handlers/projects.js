import { get, select, put, destroy } from '../lib/simpledb'
import { isAdmin } from '../lib/permissions'

export const list = () =>
  select('Project', `SELECT * FROM Project`)
    .then(projects => ({ data: projects, statusCode: 200 }))

export const save = ({ body }, auth) =>
  isAdmin(auth)
    .then(() => put('Project', body))
    .then(project => ({ data: project, statusCode: 200 }))

export const findOne = ({ pathParams: { id } }) =>
  get('Project', id)
    .then(project => ({ data: project }))

export const remove = ({ pathParams: { id } }, auth) =>
  isAdmin(auth)
    .then(() => destroy('Project', id))
    .then(data => ({ statusCode: 201 }))