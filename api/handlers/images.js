import { getPolicy, getImage } from '../lib/images'
import { v4 as uuid } from 'uuid'
import config from '../../config'
import { isAdmin } from '../lib/permissions'

const extensions = {
  'image/jpeg': 'jpg',
  'image/png': 'png',
  'image/svg+xml': 'svg'
}

export const create = ({ body }, auth) =>
  isAdmin(auth).then(() => {
    const image = { ...body, extension: extensions[body.type], id: uuid() }
    return {
      data: {
        ...image,
        ...getPolicy(image),
        awsKey: config.aws.key
      }
    }
  })

exports.findOne = ({ pathParams: { id }, queryString: { width, height } }) =>
  getImage(id, width, height)
    .then(path => ({
      headers: { 'Location': path, 'Content-Type': 'text/html' },
      statusCode: 302
    }))
