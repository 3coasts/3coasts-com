
const url = (path, params) => {
  const exp = new RegExp(/\{[a-zA-Z0-0\-]+\}/g)
  const matches = path.match(exp) || []
  return matches.reduce((url, match) => {
    url = url.replace(match, params[match.replace(/[\{\}]/g,'')])
    return url
  }, path)
}

export default url