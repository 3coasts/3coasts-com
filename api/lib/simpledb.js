/* globals config */

import AWS from 'aws-sdk'
import { v4 as uuid } from 'uuid'
import reduce from 'async/reduce'
import Joi from 'joi'
import models from '../models'
import config from '../../config'

import isDate from 'lodash/isDate'
import isObject from 'lodash/isObject'
import isArray from 'lodash/isArray'
import isNumber from 'lodash/isNumber'
import isUndefined from 'lodash/isUndefined'
import isBoolean from 'lodash/isBoolean'
import isString from 'lodash/isString'

const production = {
  accessKeyId: config.aws.key,
  secretAccessKey: config.aws.secret,
  region: 'us-east-1'
}

AWS.config.setPromisesDependency()

const db = new AWS.SimpleDB({ ...production, apiVersion: '2009-04-15' })
const s3 = new AWS.S3({ ...production, apiVersion: '2006-03-01' })

const toJson = (item) => {
  return new Promise((resolve, reject) => {
    if (!item.Attributes) return resolve()
    reduce(item.Attributes, {}, (obj, { Value, Name }, cb) => {
      if (Value instanceof Buffer) {
        Value = Value.toString()
      }
      if (Value.indexOf('s3://') === 0) {
        s3.getObject({
          Bucket: 'threecoasts-large-attributes',
          Key: Value.replace('s3://', '')
        }, (err, { Body }) => {
          cb(err, { ...obj, [Name]: Body.toString() })
        })
      } else if (Name === 'createdAt') {
        cb(null, { ...obj, [Name]: new Date(Number(Value)).getTime() })
      } else if (Name === 'updatedAt') {
        cb(null, { ...obj, [Name]: new Date(Number(Value)).getTime() })
      } else {
        cb(null, { ...obj, [Name]: Value })
      }
    }, (err, results) => {
      if (err) return reject(err)
      return resolve(results)
    })
  })
}

const stringify = (obj) => new Promise((resolve, reject) => {
  reduce(Object.keys(obj), {}, (newObj, x, cb) => {
    if (isUndefined(obj[x])) {
      cb(null, { ...newObj, [x]: '' })
    } else if (isDate(obj[x])) {
      cb(null, { ...newObj, [x]: String(obj[x].getTime()) })
    } else if (isNumber(obj[x])) {
      cb(null, { ...newObj, [x]: String(obj[x]) })
    } else if (isArray(obj[x]) || isObject(obj[x])) {
      var value = JSON.stringify(obj[x])
      if (value.length > 1024) {
        s3.putObject({
          Bucket: 'threecoasts-large-attributes',
          Key: `${obj.id}/${x}`,
          Body: value
        }, (err) => {
          cb(err, { ...newObj, [x]: `s3://${obj.id}/${x}` })
        })
      } else {
        cb(null, { ...newObj, [x]: value })
      }
    } else if (isBoolean(obj[x])) {
      cb(null, { ...newObj, [x]: obj[x] === true ? 'true' : 'false' })
    } else if (isString(obj[x])) {
      if (obj[x].length > 1024) {
        s3.putObject({
          Bucket: 'threecoasts-large-attributes',
          Key: `${obj.id}/${x}`,
          Body: obj[x]
        }, (err) => {
          cb(err, { ...newObj, [x]: `s3://${obj.id}/${x}` })
        })
      } else {
        cb(null, { ...newObj, [x]: obj[x] })
      }
    } else {
      cb(null, { ...newObj, [x]: obj[x] })
    }
  }, (err, results) => {
    if (err) return reject(err)
    resolve(results)
  })
})

export const get = (DomainName, ItemName) => {
  return db.getAttributes({
    DomainName,
    ItemName,
    ConsistentRead: true
  }).promise().then((obj = {}) => {
    if (obj === null) throw new Error('Error in SimpleDB get request.')
    if (!obj.Attributes.length) throw new Error('Not found')
    return obj
  }).then(toJson).then(models[DomainName].parse)
}

export const select = (table, SelectExpression, nextToken = null) => {
  return db.select({
    SelectExpression,
    NextToken: nextToken,
    ConsistentRead: true
  }).promise().then(({ Items, NextToken }) => {
    if (!Items) return []
    return Promise.all(Items.map(toJson)).then(items => {
      items = items.map(models[table].parse)
      items._next = NextToken
      return items
    })
  })
}

export const put = (DomainName, attributes) => {
  attributes.id = attributes.id || uuid()
  if (!attributes.createdAt) {
    attributes.createdAt = new Date().getTime()
  }
  attributes.updatedAt = new Date().getTime()
  const valid = Joi.validate(attributes, models[DomainName].schema, { abortEarly: false })
  if (valid.error) return Promise.reject(valid.error)
  return stringify(valid.value)
    .then(stringified => {
      return db.putAttributes({
        DomainName,
        ItemName: stringified.id,
        Attributes: Object.keys(stringified).filter(x => stringified[x] !== null).map(Name => {
          return {
            Name,
            Value: stringified[Name] || '',
            Replace: true
          }
        })
      }).promise().then(() => {
        return get(DomainName, attributes.id)
      })
    })
}

export const destroy = (DomainName, ItemName) => {
  return db.deleteAttributes({
    DomainName,
    ItemName
  }).promise()
}
