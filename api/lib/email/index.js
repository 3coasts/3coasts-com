import React from 'react'
import isArray from 'lodash/isArray'
import renderFullPage from './templates/render-email'
import { renderToString } from 'react-dom/server'
import AWS from '../aws'
import config from '../../../config'

const ses = new AWS.SES()

const email = (Template, args) => {
  return ses.sendEmail({
    Destination: {
      ToAddresses: isArray(args.to) ? args.to : [args.to]
    },
    Message: {
      Body: { Html: { Data: renderFullPage(renderToString(<Template { ...args } />)) } },
      Subject: { Data: args.subject }
    },
    Source: config.fromAddress
  }).promise()
}

export default email
