module.exports = (html) => {
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta name="viewport" content="width=device-width"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <style>
          /* -------------------------------------
          GLOBAL
          ------------------------------------- */
          * {
            margin:0;
            padding:0;
          }
          * { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }
          img {
            max-width: 100%;
          }
          .collapse {
            margin:0;
            padding:0;
          }
          body {
            -webkit-font-smoothing:antialiased;
            -webkit-text-size-adjust:none;
            width: 100%!important;
            height: 100%;
          }
          a { color: #8D1222;}
          .btn {
            text-decoration:none;
            color: #FFF;
            background-color: #8D1222;
            padding:10px 16px;
            font-weight:bold;
            margin-right:10px;
            text-align:center;
            cursor:pointer;
            display: inline-block;
          }
          p.callout {
            padding:15px;
            background-color:#e9e9e9;
            margin-bottom: 15px;
          }
          .callout a {
            font-weight:bold;
            color: #2BA6CB;
          }
          /* -------------------------------------
          HEADER
          ------------------------------------- */
          table.head-wrap { width: 100%;}
          .header.container table td.logo { padding: 15px; }
          .header.container table td.label { padding: 15px; padding-left:0px;}
          /* -------------------------------------
          BODY
          ------------------------------------- */
          table.body-wrap { width: 100%;}
          /* -------------------------------------
          TYPOGRAPHY
          ------------------------------------- */
          h1,h2,h3,h4,h5,h6 {
          font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
          }
          h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }
          h1 { font-weight:200; font-size: 44px;}
          h2 { font-weight:200; font-size: 37px;}
          h3 { font-weight:500; font-size: 27px;}
          h4 { font-weight:500; font-size: 23px;}
          h5 { font-weight:900; font-size: 17px;}
          h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}
          .collapse { margin:0!important;}
          p, ul {
            margin-bottom: 10px;
            font-weight: normal;
            font-size:14px;
            line-height:1.6;
          }
          p.last { margin-bottom:0px;}
          ul li {
            margin-left:5px;
            list-style-position: inside;
          }
          /* ----------
          TABLES
          ------*/
          table.line-items {
            width: 100%;
            background-color: #e9e9e9;
            border-top: 2px solid #4B5155;
            font-size: 14px;
          }
          table.line-items thead td {
            background-color: #e9e9e9;
            margin: 2px;
          }
          table.line-items tr {
            margin: 1px;
          }
          table.line-items tr.last {
            margin-bottom: 2px;
          }
          table.line-items td {
            padding: 8px 10px;
            background-color: #FFFFFF;
            margin: 1px;
          }
          td.summary table.option {
            border-bottom: 1px solid #e9e9e9;
            margin-bottom: 3px;
            padding-bottom: 3px;
          }
          td.summary table.option.last {
            border-bottom: 0;
            margin: 0;
            padding: 0;
          }
          td.summary > table > tbody > tr > td {
            padding: 0;
          }
          td.summary > table > tbody > tr > td > table > tbody > tr > td {
            padding: 0;
          }
          table.option tr.price, table.option td.price {
            text-align: right;
          }
          table tr.title, table td.title {
            font-weight: bold;
          }
          /* ---------------------------------------------------
          RESPONSIVENESS
          Nuke it from orbit. It's the only way to be sure.
          ------------------------------------------------------ */
          /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
          .container {
          display:block!important;
          max-width:600px!important;
          margin:0 auto!important; /* makes it centered */
          clear:both!important;
          }
          /* This should also be a block element, so that it will fill 100% of the .container */
          .content {
          padding:15px;
          max-width:600px;
          margin:0 auto;
          display:block;
          }
          /* Let's make sure tables in the content area are 100% wide */
          .content table { width: 100%; }
          /* Be sure to place a .clear element after each set of columns, just to be safe */
          .clear { display: block; clear: both; }
          /* -------------------------------------------
          PHONE
          For clients that support media queries.
          Nothing fancy.
          -------------------------------------------- */
          @media only screen and (max-width: 600px) {
            a[class="btn"] {
              display:block!important;
              margin-bottom:10px!important;
              background-image:none!important;
              margin-right:0!important;
            }
            div[class="column"] { width: auto!important; float:none!important;}
            table.social div[class="column"] {
              width:auto!important;
            }
          }
        </style>
      </head>
      <body bgcolor="#FFFFFF">
        <table class="body-wrap">
          <tr>
            <td></td>
            <td bgcolor="#FFFFFF" class="container">
              <div class="content">
                <table>
                  <tr>
                    <td>${html}</td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
      </body>
    </html>
  `
}
