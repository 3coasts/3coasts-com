import React from 'react'

const ContactForm = ({ name, email, message }) => {
  return (
    <div>
      <h1>Message from Website</h1>
      <p><strong>Name:</strong> {name}</p>
      <p><strong>Email:</strong> {email}</p>
      <p><strong>Message</strong><br />{message}</p>
    </div>
  )
}

export default ContactForm
