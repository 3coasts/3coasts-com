import ApiBuilder from 'claudia-api-builder'
import routes from '../routes'
import render from './render'
import auth from './auth'
import config from '../../config'
import cookie from 'cookie'

const api = new ApiBuilder()

const pre = handler => req => {
  let user = null
  const respond = ({ data = '', headers = {}, statusCode = 200 }) => {
    if (statusCode === 500) {
      return new api.ApiResponse(data, { 'Content-Type': 'text/html', ...headers }, statusCode)
    } else if (req.context.path.indexOf('api/') === 1) {
      return new api.ApiResponse(data, { 'Content-Type': 'application/json', ...headers }, statusCode)
    } else {
      const body = render(req, { ...data, user })
      return new api.ApiResponse(body, { 'Content-Type': 'text/html', ...headers }, statusCode)
    }
  }
  const authorization = req.normalizedHeaders.authorization || cookie.parse((req.normalizedHeaders.cookie || '')).Authorization || ''
  return auth(authorization).then(result => {
    user = { ...result }
    return handler ? handler(req, user) : {}
  }).then(respond).catch(e => {
    return new api.ApiResponse({
      message: e.message || e,
      stack: e.stack || null
    }, {
      'Content-Type': 'text/html'
    }, 500)
  })
}


routes.forEach(({ method, path, handler = null }) => {
  switch (method) {
    case 'get':
      return api.get(path, pre(handler))
    case 'post':
      return api.post(path, pre(handler))
    case 'put':
      return api.put(path, pre(handler))
    case 'delete':
      return api.delete(path, pre(handler))
  }
})

api.get('/{a}', pre(null))
api.get('/{a}/{b}', pre(null))
api.get('/{a}/{b}/{c}', pre(null))
api.get('/{a}/{b}/{c}/{d}', pre(null))

module.exports = api
