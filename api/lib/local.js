import express from 'express'
import bodyParser from 'body-parser'
import render from './render'
import routes from '../routes'
import cookie from 'cookie'
import auth from './auth'
import api from './api'

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(`/assets`, express.static(`${__dirname}/../../assets`))


routes.concat([
  { method: 'get', path: '/{a}' },
  { method: 'get', path: '/{a}/{b}' },
  { method: 'get', path: '/{a}/{b}/{c}' },
  { method: 'get', path: '/{a}/{b}/{c}/{d}' }
]).forEach(function ({ method, path, handler }) {
  const expressPath = path.replace(/{/g, ':').replace(/}/g, '')
  app[method](expressPath, (req, res) => {
    api.proxyRouter({
      requestContext: {
        resourcePath: path,
        httpMethod: method.toUpperCase()
      },
      body: req.body,
      pathParameters: req.params,
      queryStringParameters: req.query,
      headers: req.headers
    }, {
      done: function (err, { statusCode, headers, body }) {
        if (err) return res.status(500).send(err)
        res.set(headers)
        return res.status(statusCode).send(body)
      }
    })
  })
})

app.listen(3000, () => console.log('Dev server running @ localhost:3000'))
