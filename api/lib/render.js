import React from 'react'
import ReactDOM from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { configureStore } from '../../js/store'
import App from '../../js/app'
import { renderStatic } from 'glamor/server'
import config from '../../config'
import url from './url'

const render = (req, data = {}) => {
  const store = configureStore(data)
  const { html, css, ids } = renderStatic(() => ReactDOM.renderToString(
    <Provider store={store}>
      <StaticRouter basename={config.basePath} context={{}} location={url(req.context.path, req.pathParams)}>
        <App />
      </StaticRouter>
    </Provider>
  ))
  const fullHtml = `<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="${config.assetPath}/reset.css">
        <link rel="stylesheet" type="text/css" href="${config.assetPath}/global.css">
        <link rel="stylesheet" type="text/css" href="${config.assetPath}/fontello/css/fontello.css" />
        <link href="https://fonts.googleapis.com/css?family=Eczar:700|Roboto:100,700,400" rel="stylesheet">
        <style>${css}</style>
        <script type="application/javascript">
          window.__INITIAL_STATE__ = ${JSON.stringify(store.getState())};
          window._glam = ${JSON.stringify(ids)}
        </script>
        <link rel="apple-touch-icon" sizes="57x57" href="${config.assetPath}/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="${config.assetPath}/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="${config.assetPath}/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="${config.assetPath}/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="${config.assetPath}/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="${config.assetPath}/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="${config.assetPath}/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="${config.assetPath}/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="${config.assetPath}/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="${config.assetPath}/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="${config.assetPath}/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="${config.assetPath}/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="${config.assetPath}/favicon/favicon-16x16.png">
        <link rel="manifest" href="${config.assetPath}/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="${config.assetPath}/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <script src='https://www.google.com/recaptcha/api.js'></script>
      </head>
      <body>
        <div id="app-container">${html}</div>
        <script src="${config.assetPath}/js/index.js"></script>
      </body>
    </html>
  `
  return fullHtml
}


export default render