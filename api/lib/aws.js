import AWS  from 'aws-sdk'
import config from '../../config'

AWS.config = new AWS.Config({
  accessKeyId: config.aws.key,
  secretAccessKey: config.aws.secret,
  region: 'us-east-1'
})

AWS.config.setPromisesDependency()

export default AWS

