import config from '../../config'
const S3 = require('s3-policy')
const path = require('path')
const gm = require('gm').subClass({imageMagick: true})
import AWS from './aws'
const s3 = new AWS.S3()

const mimeTypes = {
  '.jpg': 'image/jpeg',
  '.jpeg': 'image/jpeg',
  '.png': 'image/png',
  '.gif': 'image/gif'
}

const cdn = 'https://s3.amazonaws.com/threecoasts-image-uploads'

export const getImage = (file, width, height, gravity = 'North') => {
  const bucket = 'threecoasts-image-uploads'
  const key = `originals/${file}`
  const parsedKey = path.parse(file)
  const filename = `${parsedKey.name}-${width || 'W'}x${height || 'H'}x${gravity}${parsedKey.ext}`
  if (!mimeTypes[parsedKey.ext]) {
    return Promise.resolve(`${cdn}/originals/${file}`)
  }
  if (!width && !height) {
    return Promise.resolve(`${cdn}/originals/${file}`)
  }
  return s3.headObject({ Bucket: bucket, Key: `processed/${filename}` }).promise().catch(() => {
    return s3.getObject({ Bucket: bucket, Key: key }).promise().then(data => {
      return new Promise((resolve, reject) => {
        const image = gm(data.Body, key)
        if (width && height) {
          image.resize(width, height, '^').gravity(gravity).crop(width, height)
        } else {
          image.resize(width, height)
        }
        image.quality(50).toBuffer((err, buffer) => {
          if (err) return reject(err)
          s3.putObject({
            Bucket: 'threecoasts-image-uploads',
            Key: `processed/${filename}`,
            ACL: 'public-read',
            Body: buffer,
            ContentType: mimeTypes[parsedKey.ext]
          }, (err, obj) => {
            if (err) return reject(err)
            resolve(`${cdn}/processed/${filename}`)
          })
        })
      })
    })
  }).then(data => {
    return `${cdn}/processed/${filename}`
  })
}

export const getPolicy = (image) => {
  return new S3({
    length: 100000000,
    bucket: 'threecoasts-image-uploads',
    expires: new Date(Date.now() + 60000),
    key: config.aws.key,
    secret: config.aws.secret,
    acl: 'public-read',
    name: `originals/${image.id}.${image.extension}`,
    type: image.type
  })
}
