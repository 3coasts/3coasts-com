import jwt from 'jsonwebtoken'
import jwkToPem from 'jwk-to-pem'
import request from 'request'
import config from '../../config'

const iss = `https://cognito-idp.us-east-1.amazonaws.com/${config.aws.userPoolId}`

const jwks = new Promise((resolve, reject) => {
  const url = `https://cognito-idp.us-east-1.amazonaws.com/${config.aws.userPoolId}/.well-known/jwks.json`
  request({ url: url, method: 'GET', json: true }, (err, resp, body) => {
    if (err) return reject(err)
    resolve(body)
  })
})

const auth = (authorization) => {
  return new Promise((resolve, reject) => {
    if (!authorization) return resolve(null)
    jwks.then((response) => {
      const keys = response.keys
      if (!authorization) return resolve(null)
      const decoded = jwt.decode(authorization, { complete: true })
      if (decoded.payload.iss !== iss) return resolve(null)
      if (decoded.payload.token_use !== 'id') return resolve(null)
      const key = (() => {
        for (var x in keys) {
          if (keys[x].kid === decoded.header.kid) {
            return keys[x]
          }
        }
        return null
      })()
      jwt.verify(authorization, jwkToPem(key), { algorithms: [key.alg] }, (err) => {
        if (err) return resolve(null)
        const hasExpired = decoded.payload.exp * 1000 < new Date().getTime()
        if (hasExpired) return resolve(null)
        return resolve({ ...decoded.payload, authorization })
      })
    })
  })
}

export default auth
