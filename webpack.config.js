const webpack = require('webpack')
const LiveReloadPlugin = require('webpack-livereload-plugin')
const isDev = require('isdev')

module.exports = {
  context: __dirname,
  devtool: isDev ? 'inline-source-map' : false,
  entry: {
    'index': './js/index.js'
  },
  output: {
    filename: `[name].js`,
    path: `${__dirname}/assets/js/`
  },
  module: {
    rules: [{
      test: /\.js?$/,
      exclude: /node_modules/,
      use: ['babel-loader']
    }]
  },
  plugins: isDev ? [
    new LiveReloadPlugin({ appendScriptTag: true })
  ] : [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: true,
      mangle: true,
      comments: false
    })
  ]
}
