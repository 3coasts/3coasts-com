require('babel-register')

var underTest = require('../api/api');

describe('Test for Error', () => {
  var lambdaContextSpy;

  beforeEach(() => {
    lambdaContextSpy = jasmine.createSpyObj('lambdaContext', ['done']);
  });

  it('returns html', (done) => {
    underTest.proxyRouter({
      requestContext: {
        resourcePath: '/',
        httpMethod: 'GET'
      },
      headers: {
        Authorize: 'eyJraWQiOiJcL2lIb2tDQ1wvdkp4TVpzU3Q3Rmk5Tnl0d2JNVnFBRDlwczhJN1Q3S0xcL1M4PSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI5ZTQ2NzAyMi1mMzg3LTQ0YzctOWE5NC00MTQ0OWJiMDhmYmMiLCJhdWQiOiIzMzZ0ZHNvdXQxbzI3cWk3MXF2bWo1NHJqMSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTQ5MDI5NDA0NiwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tXC91cy1lYXN0LTFfV0NGclVsQVduIiwiY29nbml0bzp1c2VybmFtZSI6ImJyaUBuYmVybC5pbiIsImV4cCI6MTQ5MDI5NzY0NiwiaWF0IjoxNDkwMjk0MDQ2LCJlbWFpbCI6ImJyaUBuYmVybC5pbiJ9.seu8XbZpKkOTMO3i1OBDyPYhl7-9VDknTrDhx3ygwFrrn59ceVJK7fKd3AEJQbUqfcZKrG9lZDtFbOAbjftTDU8I5su3kdkK-npp8-xIotnipG3fQEZHB_DBdnCzt7i50pC-D073iCmU_kaGqnTVgVFKwkVNOm_GVYr_25bM4Yp16I6LDoOe-w7mOssuUjx1f4OzqNlCasUGm9kGO2njJ7Vs_yonjH4myyie8zXUc212Tzf5pLVvcsPtlnBlbwmcWIsD7PdFEXpVac2-JezRdEoSXxAXBd42LldyWLOCLYlTDeQJIwCK8O_pwJdG-r5wFYbP2vB2OF6-pbTLeGpgmA'
      }
    }, lambdaContextSpy).then(() => {
      expect(lambdaContextSpy.done).toHaveBeenCalledWith(null, jasmine.objectContaining({body:'XHCG'}));
    }).then(done, done.fail);
  });
});
