import { combineReducers } from 'redux'

export const types = {
  menuToggle: 'TOGGLE_MENU',
  messages: {
    add: 'MESSAGES_ADD'
  },
  user: {
    auth: 'USER_AUTHENTICATED',
    update: 'USER_UPDATE',
    patch: 'USER_PATCH'
  },
  projects: {
    fetched: 'PROJECTS_FETCHED',
    edit: 'PROJECT_EDITED',
    updated: 'PROJECT_UPDATED',
    added: 'PROJECT_ADDED',
    removed: 'PROJECT_REMOVED'
  }
}

const messages = (state = [], { type, message }) => {
  switch (type) {
    case types.messages.add:
      return [
        ...state,
        message
      ]
    default:
      return state
  }
}

const user = (state = {}, { type, user }) => {
  switch (type) {
    case types.user.auth:
      return { ...user }
    case types.user.patch:
      return { ...state, ...user }
    default:
      return state
  }
}

const projects = (state = [], { type, projects, project, id }) => {
  switch (type) {
    case types.projects.fetched:
      return [ ...projects ]
    case types.projects.edit:
    case types.projects.updated:
      const index = state.indexOf(state.find(x => x.id === project.id))
      if (index === -1) return state
      return [
        ...state.slice(0, index),
        { ...project, isDirty: type === types.projects.edit },
        ...state.slice(index + 1)
      ]
    case types.projects.added:
      return [ ...state, project ]
    case types.projects.removed:
      return [ ...state.filter(x => x.id !== id) ]
    default:
      return state
  }
}

const menu = (state = false, { type }) => {
  switch (type) {
    case types.menuToggle:
      return !state
    default:
      return state
  }
}

export default combineReducers({ messages, projects, user, menu })