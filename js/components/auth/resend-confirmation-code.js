import React, { Component } from 'react'
import { resendConfirmation } from '../../actions/user'

class ResendConfirmationCode extends Component {
  constructor(props) {
    super(props)
    this.state = { email: '' }
  }

  handleEmailChange(e) {
    this.setState({email: e.target.value})
  }

  handlePasswordChange(e) {
    this.setState({password: e.target.value})
  }

  handleSubmit(e) {
    e.preventDefault()
    const { dispatch } = this.props
    const email = this.state.email.trim()
    const password = this.state.password.trim()
    dispatch(resendConfirmation(email, password))
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit.bind(this)}>
        <input type="text"
               value={this.state.email}
               placeholder="Email"
               onChange={this.handleEmailChange.bind(this)}/>
        <input type="password"
               value={this.state.password}
               placeholder="Password"
               onChange={this.handlePasswordChange.bind(this)}/>
        <input type="submit"/>
      </form>
    )
  }
}

export default ResendConfirmationCode
