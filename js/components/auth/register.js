import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { register } from '../../actions/user'
import { TextInput, Textarea, Button, SecondaryAction } from '../form-elements'
import { Container, H1 } from '../'

@connect(({ user: { registered } }) => ({ registered }))
class Register extends Component {

  constructor(props) {
    super(props)
    this.state = { email: '', password: '' }
  }

  handleEmailChange(e) {
    this.setState({email: e.target.value})
  }

  handlePasswordChange(e) {
    this.setState({password: e.target.value})
  }

  handleSubmit(e) {
    e.preventDefault()
    const { dispatch } = this.props
    const email = this.state.email.trim()
    const password = this.state.password.trim()
    dispatch(register(email, password))
  }

  render() {
    const { registered } = this.props
    return (
      <Container>
        { registered ? <Redirect to='/confirm-registration' /> : null }
        <H1>Sign up</H1>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <TextInput
            label="Email"
            type="text"
            value={this.state.email}
            placeholder="Email"
            onChange={this.handleEmailChange.bind(this)}/>
          <TextInput
            label="Password"
            type="password"
            value={this.state.password}
            placeholder="Password"
            onChange={this.handlePasswordChange.bind(this)}/>
          <Button type="submit">Submit</Button>
          <SecondaryAction to="/login">Back to Sign In</SecondaryAction>
        </form>
      </Container>
    )
  }
}

export default Register
