import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, H1 } from '../'
import { TextInput, Textarea, Button, SecondaryAction } from '../form-elements'
import { resetPassword } from '../../actions/user'
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import config from '../../../config'
import { types } from '../../reducers'

class ForgotPassword extends Component {

  constructor () {
    super()
    this.state = { email: '', code: '', password: '' }
  }

  setPassword () {
    const { dispatch } = this.props
    const { email, code, password } = this.state
    dispatch(resetPassword(email, password, code))
  }

  componentDidMount () {
    const { email } = this.props
    this.setState({ email })
  }

  render () {
    const { email, sub } = this.props
    return (
      <Container>
        <H1>Reset Password</H1>
        { !email ? <Redirect to="/forgot-password" /> : null }
        { sub ? <Redirect to="/" /> : null }
        <TextInput
          onChange={(e) => this.setState({ email: e.target.value })}
          value={this.state.email}
          name="email"
          placeholder="Email"
          label="Email" />
        <TextInput
          onChange={(e) => this.setState({ code: e.target.value })}
          name="code"
          value={this.state.code}
          placeholder="Code"
          label="Code" />
        <TextInput
          onChange={(e) => this.setState({ password: e.target.value })}
          value={this.state.password}
          name="password"
          type="password"
          placeholder="New Password"
          label="New Password" />
        <Button onClick={() => this.setPassword()}>Reset Password</Button>
      </Container>
    )
  }
}

export default connect(({ user: { email, sub } }) => ({ email, sub }))(ForgotPassword)
