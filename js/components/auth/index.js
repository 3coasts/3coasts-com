import Register from './register'
import Login from './login'
import ResendConfirmationCode from './resend-confirmation-code'
import ConfirmRegistration from './confirm-registration'

export {
  Register,
  Login,
  ResendConfirmationCode,
  ConfirmRegistration
}