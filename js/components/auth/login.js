import React, { Component } from 'react'
import { connect } from 'react-redux'
import { login } from '../../actions/user'
import { TextInput, Button, SecondaryAction } from '../form-elements'
import { Container, H1 } from '../'
import { Redirect } from 'react-router-dom'

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { email: '', password: '' };
  }

  handleSubmit(e) {
    e.preventDefault();
    const { dispatch } = this.props
    const Username = this.state.email.trim()
    const Password = this.state.password.trim()
    dispatch(login(Username, Password))
  }

  render() {
    const { user: { sub } } = this.props
    return (
      <Container>
        <H1>Login</H1>
        { sub ? <Redirect to="/user" /> : null }
        <form onSubmit={(e) => this.handleSubmit(e)}>
          <TextInput type="text"
            name="email"
            label="Email"
            value={this.state.email}
            placeholder="Email"
            onChange={(e) => this.setState({ email: e.target.value })}/>
          <TextInput type="password"
            name="password"
            label="Password"
            type="password"
            value={this.state.password}
            placeholder="Password"
            onChange={(e) => this.setState({ password: e.target.value })}/>
          <Button type="submit">Log In</Button>
          <SecondaryAction to="/forgot-password">Forgot Password?</SecondaryAction>
          <SecondaryAction to="/signup">Sign Up</SecondaryAction>
        </form>
      </Container>
    );
  }
}

export default connect(({ user }) => ({ user }))(Login)
