import React, { Component } from 'react'
import { connect } from 'react-redux'
import { confirmRegistration, resendConfirmation } from '../../actions/user'
import { TextInput, Textarea, Button, SecondaryAction } from '../form-elements'
import { Container, H1 } from '../'
import { Redirect } from 'react-router-dom'
import { types } from '../../reducers'

@connect(({ user }) => ({ user }))
class ConfirmRegistration extends Component {

  componentDidMount () {
    const { user: { email } } = this.props
    this.setState({ email })
  }

  handleEmailChange(e) {
    const { dispatch, user } = this.props
    dispatch({ type: types.user.patch, user: { ...user, email: e.target.value } });
  }

  handleCodeChange(e) {
    const { dispatch, user } = this.props
    dispatch({ type: types.user.patch, user: { ...user, code: e.target.value } });
  }

  handleSubmit(e) {
    e.preventDefault();
    const { dispatch, user: { email, code } } = this.props
    dispatch(confirmRegistration(email, code))
  }

  resend () {
    const { dispatch, user: { email } } = this.props
    dispatch(resendConfirmation(email))
  }

  render() {
    const { user: { confirmed, email, code } } = this.props
    return (
      <Container>
        { confirmed ? <Redirect to="/login" /> : null }
        <H1>Confirm Registration</H1>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <TextInput
            type="text"
            value={email}
            placeholder="Email"
            label="Email"
            onChange={this.handleEmailChange.bind(this)}
          />
          <TextInput
            name="code"
            type="password"
            value={code}
            placeholder="Code"
            label="Code"
            onChange={this.handleCodeChange.bind(this)}
          />
          <Button type="submit">Submit</Button>
          { email ? <Button secondary onClick={() => this.resend()}>Resend</Button> : null }
        </form>
      </Container>
    );
  }
}

export default ConfirmRegistration
