import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, H1 } from '../'
import { TextInput, Textarea, Button, SecondaryAction } from '../form-elements'
import { Redirect } from 'react-router-dom'
import { types } from '../../reducers'
import { forgotPassword } from '../../actions/user'

@connect(({ user: { reset } }) => ({ reset }))
class ForgotPassword extends Component {

  constructor () {
    super()
    this.state = { email: '' }
  }

  resetPassword () {
    const { dispatch } = this.props
    dispatch(forgotPassword(this.state.email))
  }

  render () {
    const { reset } = this.props
    return (
      <Container>
        <H1>Forgot Password</H1>
        <TextInput
          onChange={(e) => this.setState({ email: e.target.value })}
          value={this.state.email}
          name="email"
          placeholder="Email"
          label="Email" />
        <Button onClick={() => this.resetPassword()}>Send Reset Instructions</Button>
        { reset ? <Redirect to="/reset-password" /> : null }
      </Container>
    )
  }
}

export default ForgotPassword
