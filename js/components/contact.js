import React, { Component } from 'react'
import { Container, H1, H4 } from './'
import { TextInput, Textarea, Button } from './form-elements'
import Recaptcha from 'react-recaptcha'
import config from '../../config'
import axios from 'axios'

class Contact extends Component {
  constructor () {
    super()
    this.state = { error: false, sent: false, verified: false, email: '', name: '', message: '' }
  }

  verified (verified) {
    this.setState({ verified })
  }

  submit () {
    axios({
      method: 'POST',
      url: `${config.basePath}/contact`,
      data: this.state
    }).then(({ status, data }) => {
      if (status !== 200) {
        this.setStatus({ error: data })
      } else {
        this.setState({ sent: true })
      }
    })
  }

  render () {
    const { verified, sent, error } = this.state
    return (
      <Container className="contact-component">
        <H1>Contact</H1>
        { sent ? <H4>Your email has been sent</H4> : null }
        { error ? <H4>{error}</H4> : null }
        <TextInput onChange={(e) => this.setState({ name: e.target.value })}
          label="Name" type="text" name="name" />
        <TextInput onChange={(e) => this.setState({ email: e.target.value })}
          label="Email" type="text" name="email" />
        <Textarea onChange={(e) => this.setState({ message: e})}
          label="Message" name="message" />
        <Recaptcha
          sitekey={config.captcha.siteKey}
          verifyCallback={(e) => this.verified(e)}
          onloadCallback={() => {}}
          expiredCallback={() => this.verified(false)}
          render="explicit" />
        { verified ? <Button onClick={() => this.submit()}>Submit</Button> : null }
      </Container>
    )
  }
}

export default Contact
