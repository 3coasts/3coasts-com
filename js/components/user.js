import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, H1, ButtonDiv } from './index'
import { SecondaryAction } from './form-elements'
import { signOut } from '../actions/user'
import { types } from '../reducers'
import { Redirect } from 'react-router-dom'
import config from '../../config'

class User extends Component {
  signOut () {
    const { dispatch, user: { email } } = this.props
    dispatch(signOut(email))
  }
  render () {
    const { user: { email, sub } } = this.props
    return (
      <Container>
        <H1>{email}</H1>
        { !sub ? <Redirect to="/login" /> : null }
        <SecondaryAction to="/admin/projects">Manage Projects</SecondaryAction>
        <ButtonDiv onClick={() => this.signOut()}>Sign Out</ButtonDiv>
      </Container>
    )
  }
}

export default connect(({ user }) => ({ user }))(User)
