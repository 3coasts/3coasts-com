import React from 'react'
import { Container, H4, P2 } from './'
import { styled } from 'glamor/styled'

const Padding = styled.div`
  padding: 50px 0;
`
const A = styled.a`
  font-size: 18px;
  font-family: Roboto;
  font-weight: 100;
  line-height: 24px;
  color: rgba(255, 255, 255, 0.7);
  margin: 10px 0
`

const Footer = () => {
  return (
    <Container>
      <Padding>
        <H4>Address</H4>
        <P2>1215 Prytania St., Suite 415<br />New Orleans, LA 70130</P2>
        <H4>Contact</H4>
        <P2>Brian Berlin<br />Mobile: (225) 907-0291<br />Email: <A href="mailto:bri@nberl.in">bri@nberl.in</A></P2>
      </Padding>
    </Container>
  )
}

export default Footer
