import React, { Component } from 'react'
import { connect } from 'react-redux'
import { H1, H3, Container, Grid, Card, CardH1, CardH2, CardP, CardA } from './'
import ImgAsset from './image/asset'
import config from '../../config'
import { Display } from './editor'

class Project extends Component {
  componentDidMount () {

  }
  render () {
    const { project: { id, title, subtitle, body, image } } = this.props
    return (
      <Container className="project-component">
        <H1>{title}</H1>
        <H3>{subtitle}</H3>
        <ImgAsset image={image} width='1000' />
        <Display components={body} />
      </Container>
    )
  }
}

export default connect(({ projects }, { match: { params: { id } } }) => ({ project: projects.find(x => x.id === id) }))(Project)