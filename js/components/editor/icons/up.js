import React from 'react'

const Up = () => {
  return (
    <svg version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px" y="0px" width="11.4px" height="6.4px" viewBox="0 0 11.4 6.4" enableBackground="new 0 0 11.4 6.4">
      <defs>
      </defs>
      <path d="M11.4,5.7c0,0.2-0.1,0.4-0.2,0.5c-0.1,0.1-0.3,0.2-0.5,0.2h-10c-0.2,0-0.4-0.1-0.5-0.2C0.1,6.1,0,5.9,0,5.7
        c0-0.2,0.1-0.4,0.2-0.5l5-5C5.4,0.1,5.5,0,5.7,0c0.2,0,0.4,0.1,0.5,0.2l5,5C11.4,5.4,11.4,5.5,11.4,5.7z"/>
    </svg>
  )
}

export default Up