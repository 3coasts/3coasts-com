import React, { Component } from 'react'

class Down extends Component {
  render () {
    return (
      <svg version="1.1"
        xmlns="http://www.w3.org/2000/svg"
        x="0px" y="0px" width="11.4px" height="6.4px" viewBox="0 0 11.4 6.4" enableBackground="new 0 0 11.4 6.4">
        <defs>
        </defs>
        <path d="M11.2,0.2c0.1,0.1,0.2,0.3,0.2,0.5s-0.1,0.4-0.2,0.5l-5,5C6.1,6.4,5.9,6.4,5.7,6.4S5.4,6.4,5.2,6.2l-5-5C0.1,1.1,0,0.9,0,0.7s0.1-0.4,0.2-0.5S0.5,0,0.7,0h10C10.9,0,11.1,0.1,11.2,0.2z"/>
      </svg>
    )
  }
}

export default Down