import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { types } from '../../../reducers'
import { v4 as uuid } from 'uuid'
import ImageUpload from '../../image/upload'
import ImageAsset from '../../image/asset'

export class Display extends Component {
  static propTypes = {
    component: PropTypes.shape({
      value: PropTypes.string,
    }),
  }

  render () {
    const { component: { value } } = this.props
    return (
      <div className="image-component">
        <ImageAsset image={value} />
      </div>
    )
  }
}

export class Edit extends Component {

  static propTypes = {
    component: PropTypes.shape({
      value: PropTypes.string,
    }),
  }

  onChange ({ value }) {
    const { onChange, component } = this.props
    onChange({ ...component, value })
  }

  render () {
    const { component: { value } } = this.props
    return (
      <ImageUpload onChange={image => this.onChange({ image })}>
        <a>Click Here</a>
      </ImageUpload>
    )
  }

}
