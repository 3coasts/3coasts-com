import * as markdown from './markdown'
import * as youtube from './youtube'
import * as image from './image'

export default {
  markdown,
  youtube,
  image
}