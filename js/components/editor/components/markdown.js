import React, { Component, PropTypes } from 'react'
import { v4 as uuid } from 'uuid'
import { markdown } from 'markdown'
import { styled } from 'glamor/styled'

export class Display extends Component {
  static propTypes = {
    component: PropTypes.shape({
      id: PropTypes.string,
      value: PropTypes.string.isRequired,
    }).isRequired,
  }

  render () {
    const { component: { value, id } } = this.props
    return (
      <div className="markdown-component"
        dangerouslySetInnerHTML={{ __html: markdown.toHTML(value) }}
      ></div>
    )
  }
}

export class Edit extends Component {
  static propTypes = {
    component: PropTypes.shape({
      id: PropTypes.string,
      value: PropTypes.string,
    }),
  }

  onSubmit (e) {
    const { component, onChange } = this.props
    e.preventDefault()
    onChange({
      ...component,
      value: this.refs.textarea.value
    })
  }

  cancel () {
    const { component, onChange } = this.props
    onChange({ ...component, edit: false })
  }

  render () {
    const { component: { value } } = this.props
    const Textarea = styled.textarea`
      background-color: #1c5c28;
      border: 1px solid lighten(#1c5c28, 10%);
      width: 100%;
      color: #fff;
      min-height: 300px;
      font-size: 20px;
      font-family: Roboto;
      font-weight: 100;
      line-height: 27px;
      color: #fff;
    `
    return (
      <form onSubmit={(e) => this.onSubmit(e)}>
        <Textarea ref="textarea" value={value}></Textarea>
        <button type="submit">Save</button>
        <button onClick={() => this.cancel()}>Cancel</button>
      </form>
    )
  }

}
