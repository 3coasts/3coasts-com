import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { types } from '../../../reducers'
import { v4 as uuid } from 'uuid'
import { markdown } from 'markdown'
import { styled } from 'glamor/styled'
import axios from 'axios'

const Iframe = styled.iframe`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
`

const IframeContainer = styled.div`
	position: relative;
	padding-bottom: 56.25%;
	height: 0;
`

export class Display extends Component {

  static propTypes = {
    component: PropTypes.shape({
      id: PropTypes.string,
      ytid: PropTypes.string
    }),
  }

  render () {
    const { component: { ytid, id } } = this.props
    return (
      <IframeContainer className="youtube-component">
        <Iframe src={`https://www.youtube.com/embed/${ytid}`} frameBorder="0" allowFullScreen />
      </IframeContainer>
    )
  }
}

export class Edit extends Component {

  static propTypes = {
    component: PropTypes.shape({
      id: PropTypes.string,
      ytid: PropTypes.string,
    }),
  }

  parseUrl (url) {
    if (/youtu\.?be/.test(url)) {

      // Look first for known patterns
      var i;
      var patterns = [
        /youtu\.be\/([^#\&\?]{11})/,  // youtu.be/<id>
        /\?v=([^#\&\?]{11})/,         // ?v=<id>
        /\&v=([^#\&\?]{11})/,         // &v=<id>
        /embed\/([^#\&\?]{11})/,      // embed/<id>
        /\/v\/([^#\&\?]{11})/         // /v/<id>
      ];

      // If any pattern matches, return the ID
      for (i = 0; i < patterns.length; ++i) {
        if (patterns[i].test(url)) {
          return patterns[i].exec(url)[1];
        }
      }

      // If that fails, break it apart by certain characters and look
      // for the 11 character key
      var tokens = url.split(/[\/\&\?=#\.\s]/g);
      for (i = 0; i < tokens.length; ++i) {
        if (/^[^#\&\?]{11}$/.test(tokens[i])) {
          return tokens[i];
        }
      }
    } else {
      return null
    }
  }

  onSubmit (e) {
    const { onChange, component } = this.props
    e.preventDefault()
    const ytid = this.parseUrl(this.refs.input.value)
    if (!ytid) return null
    onChange({ ...component, ytid })
  }

  render () {
    return (
      <form onSubmit={(e) => this.onSubmit(e)}>
        <input ref="input"></input>
        <button type="submit">Add</button>
      </form>
    )
  }

}
