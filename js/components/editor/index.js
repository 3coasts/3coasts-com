import React, { Component } from 'react'
import { connect } from 'react-redux'
import Components from './components'
import { types } from '../../reducers'
import { ButtonDiv, NoList } from '../index'
import Down from './icons/down'
import Up from './icons/up'
import Remove from './icons/remove'
import Edit from './icons/edit'
import { styled } from 'glamor/styled'
import { v4 as uuid } from 'uuid'

const ItemContainer = styled.div`
  position: relative;
`

const Items = styled.ul`
  position: absolute;
  z-index: 1;
  top: 0;
  left: -60px;
  padding: 0;
  list-style: none;
  width: 50px;
`

const Item = styled.li`
  background-color: #6fbd44;
  padding: 10px;
  align-items: center;
  text-align: center;
  width: 40px;
  &:hover {
    background-color: #fff;
  }
  &:first-child {
    border-radius: 4px 4px 0 0
  }
  &:last-child {
    border-radius: 0 0 4px 4px
  }
  > svg > path {
    fill: #fff;
  }
  &:hover > svg > path {
    fill: #6fbd44;
  }
`

const NullComponent = () => {
  return null
}

export class Display extends Component {

  displayComponent (component) {
    return Components[component.type].Display || NullComponent
  }

  render () {
    const { components } = this.props
    return (
      <div className="editor-display-component">
        { components.map(component => {
          const DisplayComponent = this.displayComponent(component)
          return <DisplayComponent
            component={component}
            key={component.id}
          />
        }) }
      </div>
    )
  }
}

export class Editor extends Component {

  getComponent (component) {
    return Components[component.type][component.edit ? 'Edit' : 'Display'] || NullComponent
  }

  remove (id) {
    const { onChange, components } = this.props
    if (window.confirm('Are you sure?')) {
      onChange([ ...components.filter(x => x.id !== id) ])
    }
  }

  add (type) {
    const { components, onChange } = this.props
    onChange([ ...components, { id: uuid(), edit: true, type } ])
  }

  edit (id) {
    const { onChange, components } = this.props
    onChange(components.map(x => x.id !== id ? x : { ...x, edit: true }))
  }

  update (component) {
    const { onChange, components } = this.props
    onChange(components.map(x => x.id !== component.id ? x : { ...component, edit: false }))
  }

  move(id, direction) {
    const { components, onChange } = this.props
    const index = components.findIndex(x => x.id === id)
    const atTheBottom = direction === 'down' && index === components.length - 1
    const atTheTop = direction === 'up' && index === 0
    if (atTheBottom || atTheTop) return
    onChange(direction === 'up' ? [
      ...components.slice(0, index - 1),
      components[index],
      components[index - 1],
      ...components.slice(index + 1)
    ] : [
      ...components.slice(0, index),
      components[index + 1],
      components[index],
      ...components.slice(index + 2)
    ])
  }

  render() {
    const { onChange, components = [] } = this.props
    return (
      <div>
        <NoList>
          <li><ButtonDiv onClick={() => this.add('markdown')}>Markdown</ButtonDiv></li>
          <li><ButtonDiv onClick={() => this.add('youtube')}>Youtube Video</ButtonDiv></li>
          <li><ButtonDiv onClick={() => this.add('image')}>Image</ButtonDiv></li>
        </NoList>
        <div className="preview">
          { components.map(component => {
            const Component = this.getComponent(component)
            return (
              <ItemContainer key={component.id}>
                <Items>
                  <Item onClick={() => this.edit(component.id)}><Edit /></Item>
                  <Item onClick={() => this.remove(component.id)}><Remove /></Item>
                  <Item onClick={() => this.move(component.id, 'up')}><Up /></Item>
                  <Item onClick={() => this.move(component.id, 'down')}><Down /></Item>
                </Items>
                <div className="editor-display-component">
                  <Component component={component} onChange={x => this.update(x)} />
                </div>
              </ItemContainer>
            )
          }) }
        </div>
      </div>
    )
  }
}
