import React from 'react'
import { Link } from 'react-router-dom'
import { styled } from 'glamor/styled'

const RegularInput = styled.input`
  background-color: #fff;
  appearance: none;
  border: 1px solid #123d16;
  padding: 8px 8px 12px 8px;
  font-size: 16px;
  font-family: Roboto;
  font-weight: lighter;
  line-height: 27px;
  color: #333;
  border-radius: 3px;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
  max-width: 100%;
  width: 100%;
  outline: none;
  &:focus {
    padding-bottom: 8px;
    border-bottom: 5px solid #6fbd44;
  }
`

const InputContainer = styled.label`
  color: #fff;
  margin: 20px 0;
  display: block;
`

const InputLabel = styled.div`
  font-size: 14px;
  font-family: Roboto;
  font-weight: bold;
  line-height: 27px;
  color: #fff;
  margin: 20px 0 5px;
  text-transform: uppercase;
`

const TextareaEl = styled.textarea`
  background-color: #fff;
  appearance: none;
  border: 1px solid #123d16;
  padding: 8px 8px 12px 8px;
  font-size: 16px;
  font-family: Roboto;
  font-weight: lighter;
  line-height: 27px;
  color: #333;
  border-radius: 3px;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
  max-width: 100%;
  width: 100%;
  min-height: 200px;
  outline: none;
  &:focus {
    padding-bottom: 8px;
    border-bottom: 5px solid #6fbd44;
  }
`

export const Button = styled.button`
  font-family: Roboto;
  padding: 20px;
  font-size: 16px;
  text-transform: uppercase;
  font-weight: bold;
  appearance: none;
  border: none;
  border-radius: 3px;
  margin: 20px 20px 20px 0;
  outline: none;
  color: #fff;
  background-color: ${({ secondary }) => secondary ? '#1c320f' : '#6fbd44' };
  &:hover {
    background-color: #fff;
    color: #6fbd44
  }
  &:active {
    color: #fff;
    background-color: ${({ secondary }) => secondary ? '#1c320f' : '#6fbd44' };
  }
`
export const SecondaryAction = styled(Link)`
  font-family: Roboto;
  display: inline-block;
  text-decoration: none;
  outline: none;
  padding: 20px;
  font-size: 16px;
  text-transform: uppercase;
  font-weight: bold;
  appearance: none;
  border: none;
  border-radius: 3px;
  margin: 20px 20px 20px 0;
  color: #fff;
  background-color: #1c320f;
  &:hover {
    background-color: #fff;
    color: #6fbd44;
  }
  &:active {
    color: #fff;
    background-color: #1c320f;
  }
`

export const TextInput = ({ type = 'text', disabled, value, label, placeholder = '', name, onChange }) => {
  return (
    <InputContainer>
      <InputLabel>{label}</InputLabel>
      <RegularInput type={type}
        disabled={disabled}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        name={name} />
    </InputContainer>
  )
}

export const Textarea = ({ value, label, name, onChange, placeholder }) => {
  return (
    <InputContainer>
      <InputLabel>{label}</InputLabel>
      <TextareaEl
        value={value}
        name={name}
        placeholder={placeholder}
        onChange={(e) => onChange(e.target.value)} />
    </InputContainer>
  )
}