import { styled } from 'glamor/styled'


export const TitleInput = styled.input`
  font-size: 50px;
  font-family: Eczar;
  font-weight: 700;
  line-height: 54px;
  color: #fff;
  margin: 20px 0;
  background: transparent;
  border: none;
  appearance: none;
  outline: none
`

