import React from 'react'
import { Container, H1, H3, H4, P } from './'

const About = () => {
  return (
    <Container className="about-component">
      <H1>About</H1>
      <P>3Coasts is a design and development company located in the heart of New Orleans. Our focus is simple: We love to build beautiful, state-of-the-art web applications. Our team came together in July 2011 from diverse backgrounds, combining over twenty years of experience with open-source web technologies.</P>
      <P>Like most geeks, we’re crazy about effectiveness and efficiency, and we constantly refine our processes to save time, energy, and money. Beyond focusing on making websites that look great and work well, we’re ferociously committed to staying on the cutting edge of the web. Today, that’s no small errand. The ways we use the web are constantly--even exponentially--multiplying. We try to surf the crest of this wave and do it in style.</P>
      <P>At the end of the day, we ourselves are some of the most ferocious technology users out there. We consistently bring our own experiences—both the joys and frustrations of living and working in a profoundly wired world—back to the work we do every day. The nature of the web dictates that our job is never finished, but we prefer it that way. It’s the environment we thrive in. When you partner with us, the web will be where you thrive, too.</P>

      <H3>Staff</H3>
      <H4>Brian Berlin</H4>
      <P>Owner / Full Stack Web</P>
      <P>Brian is a full stack developer with over a decade of experience building web applications. His previous apps have been featured by Forbes, Tech Crunch, CNN, Fox News, NBC, Product Hunt, and other media.</P>

      <H4>Dan Horne</H4>
      <P>Web Developer</P>
      <P>Dan Horne has been working with us in some capacity from the start, and in July 2012 he decided to team up with us full-time from his home in Asheville. He was previously COO of TAIS, an ISP that serviced the downtown Asheville area. He has both broad and deep experience in all kinds of internet technologies, and after years providing custom development for a variety of clients, he decided to leave his previous job to focus on web development. He is our resident Wordpress guru and a formidable coder, tackling the lion's share of our development day in and day out.</P>

    </Container>
  )
}

export default About
