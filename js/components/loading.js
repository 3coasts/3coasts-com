import React, { Component } from 'react'
import { styled } from 'glamor/styled'
import { keyframes } from 'styled-components'

const Loader = styled.div`
  position: relative;
  margin: 0 auto;
  &:before {
    content: '';
    display: block;
    padding-top: 100%;
  }
`

const rotate = keyframes`
  100% {
    transform: rotate(360deg);
  }
`
const dash = keyframes`
  0% {
    stroke-dasharray: 1, 200;
    stroke-dashoffset: 0;
  }
  50% {
    stroke-dasharray: 89, 200;
    stroke-dashoffset: -35px;
  }
  100% {
    stroke-dasharray: 89, 200;
    stroke-dashoffset: -124px;
  }
`

class Loading extends Component {
  render () {
    const { size = '100px', color = '#fff', style } = this.props
    return (
      <Loader style={{ width: size, height: size, ...style }}>
        <svg style={{
          animation: `${rotate} 2s linear infinite`,
          height: '100%',
          transformOrigin: 'center center',
          width: '100%',
          position: 'absolute',
          top: '0',
          bottom: '0',
          left: '0',
          right: '0',
          margin: 'auto'
        }} viewBox="25 25 50 50">
          <circle style={{
            strokeDasharray: '1, 200',
            strokeDashoffset: '0',
            animation: `${dash} 1.5s ease-in-out infinite`,
            strokeLinecap: 'round',
            stroke: color
          }} cx="50" cy="50" r="20" fill="none" stroke="transparent" strokeWidth="2" strokeMiterlimit="10"/>
        </svg>
      </Loader>
    )
  }
}

export default Loading
