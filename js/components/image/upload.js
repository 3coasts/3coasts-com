/* globals config, FormData */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { types } from '../../reducers'
import axios from 'axios'
import config from '../../../config'
import { styled } from 'glamor/styled'

const InvisibleInput = styled.input`
  display: none;
`

class ImageUploader extends Component {

  constructor () {
    super()
    this.state = { percent: 0 }
  }
  imageDimensions (file) {
    return new Promise((resolve, reject) => {
      var img = document.createElement("img")
      img.classList.add("obj")
      img.file = file
      var reader = new FileReader()
      reader.onload = ((aImg) => e => (aImg.src = e.target.result))(img)
      reader.readAsDataURL(file)
      img.onload = () => resolve({ width: img.width, height: img.height })
    })
  }
  uploadImage (e) {
    const { onChange, dispatch } = this.props
    const file = e.target.files[0]
    axios({
      method: 'POST',
      url: '/api/images',
      data: {
        type: file.type,
        extension: file.name.split('.').pop()
      }
    }).then(({ data }) => {
      const fd = new FormData()
      fd.append('acl', 'public-read')
      fd.append('key', `originals/${data.id}.${data.extension}`)
      fd.append('Content-Type', data.type)
      fd.append('Content-Length', '0')
      fd.append('AWSAccessKeyId', data.awsKey)
      fd.append('policy', data.policy)
      fd.append('signature', data.signature)
      fd.append('file', file)

      return axios({
        url: `https://threecoasts-image-uploads.s3.amazonaws.com/`,
        method: 'POST',
        data: fd,
        onUploadProgress: ({ loaded, total }) => {
          this.setState({ percent: parseInt((loaded / total) * 100) })
        }
      }, true).then(() => (data))
    }).then(({ id, extension, type }) => {
      this.imageDimensions(file).then(dimensions => onChange({ id, extension, type, ...dimensions }))
    }).catch(error => {
      dispatch({ type: types.messages.add, level: 'error', message: error.toString(), stack: error.stack })
    })
  }

  render () {
    const { image = {}, children } = this.props
    const { percent } = this.state
    return (
      <div className="image-upload-component">
        { percent > 0 && percent < 100 ? (
          <div className="progress">
            <div className="indicator" style={{ width: `${percent}%` }}></div>
          </div>
        ) : null }
        <label className="image">
          { image.id ? <img src={`${config.basePath}/api/images/${image.id}.${image.extension}?width=400`} /> : null }
          { children || null }
          <InvisibleInput name="image" type="file" required onChange={(e) => this.uploadImage(e)} />
        </label>
      </div>
    )
  }
}


export default connect()(ImageUploader)


