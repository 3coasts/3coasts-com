import React, { Component } from 'react'
import config from '../../../config'
import { styled } from 'glamor/styled'
import { keyframes } from 'styled-components'
import Loading from '../loading'

const Placeholder = () => (
  <img src={'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAADYUlEQVR4Xu3caW7qQBAEYHwM5hrc/wbmGDC3eEROxAsBL7P0UmU3f5CI3d1TnxdkKQzjOD7O5/MpXv4J5JxPw+12+5dzHi6Xi/9EB57ger2eUkqPb5CU0jB9ECg+R8Qz+5zzL8g0SqDYg7xm/gESKLYg7yfALEig2KDMXY0WQQJFF2Xp1rAKEig6KGv36U2QQJFF2frSVAQSKDIoWxhTl2KQQOlDKcGoBgmUNpRSjCaQQKlDqcFoBgmUMpRajC6QQFlHacHoBgmUeZRWDBGQQPmL0oMhBhIoPyi9GKIgUgOV3S7xtpLAEAc5KooUhgrI0VAkMdRAjoIijaEKsncUDQx1kL2iaGGYgOwNRRPDDGQvKNoYpiDsKBYY5iCsKFYYLiBsKJYYbiAsKNYYriDoKB4Y7iCoKF4YECBoKJ4YMCAoKN4YUCDeKAgYcCBeKCgYkCDWKEgYsCBWKGgY0CDaKIgY8CBaKKgYFCDSKMgYNCBSKOgYVCC9KAwYdCCtKCwYlCC1KEwYtCClKGwY1CBbKIwY9CBLKKwYuwB5R2HG2A3IE2V6Z/95qar/U58WjPqazowAAdF5vUzFJcsZZQ6AGYX6krUWPCsKLUhJ4CXbOJ/gH+0pQWqCrtkWAYcOpCXgln28cKhAeoLt2dcShwZEIlCJGto4FCCSQUrW0sCBB9EIUKOmFA40iGZwmrV7cGBBLAKz6FGLAwliGZRlrxIcOBCPgDx6LuFAgXgG49n7FQcGBCEQhBkgQBCCeB6l3rO4g3gHMHct95zJFcRz4VvfeLxmcwPxWvAWxOvfPWZ0AfFYaA2EJ4o5CBOGx43eFIQRwxrFDIQZwxLFBGQPGFYo6iB7wrBAUQXZI4Y2ihrInjE0UVRAjoChhSIOciQMDRRRkCNiSKOIgRwZQxJFBCQwfp9+9WbRDdI7QOtDP+T9ejLpAulpjByoxGyt2TSDtDaUWCxLjZaMmkBaGrGEKD1nbVbVILUNpBfIWK8msyqQmsKMwWnOXJpdMUhpQc1FsdcuybAIpKQQe1hW829luQmyVcBqIXvqs5bpKkhg6B0GS9kuggSGHsbas69ZkMDQx1hC+QAJDDuMOZQ/IIFhj/GO8h8k5zyw//iXX5wynacTIqX0GMZxfKSUZKpGla4E7vf76QtfDj7Wr5LB3QAAAABJRU5ErkJggg=='} />
)

const fadeIn = keyframes`
  0% {
    opacity: 0;
    filter: blur(30px);
  }

  100% {
    opacity: 1;
    filter: blur(0px);
  }
`;

const Img = styled.img`
  max-width: 100%;
  height: auto;
  animation: ${fadeIn} .2s linear;
`

class ImgAsset extends Component {
  constructor ({ image, width, height, gravity }) {
    super()
    const basePath = `${config.basePath}/api/images/${image.id}.${image.extension}`
    const path = `${basePath}?width=${width || ''}&height=${height || ''}&gravity=${gravity || ''}`
    this.state = {
      show: false,
      path
    }
    if (width && !height) {
      this.state.dimensions = { width, height: (image.height / image.width ) * width }
    } else if (!width && height) {
      this.state.dimensions = { height, width: (image.height / image.width ) * width }
    } else if (width && height) {
      this.state.dimensions = { height, width }
    } else if (!width && !height) {
      this.state.dimensions = { height: image.height, width: image.width }
    }
  }

  componentDidMount () {
    const { path } = this.state
    const preload = document.createElement('img')
    preload.onload = () => this.setState({ show: true })
    preload.src = path
  }

  render () {
    const { show } = this.state
    const { path, dimensions: { width, height } } = this.state
    if (!show) {
      return (
        <div style={{ width: '100%', lineHeight: '0', position: 'relative' }}>
          <svg style={{ maxWidth: '100%', height: 'auto' }} x="0px" y="0px"
            width={`${width}px`} height={`${height}px`}
            viewBox={`0 0 ${width} ${height}`}
            enableBackground={`new 0 0 ${width} ${height}`}>
            <rect fill="#333" width={`${width}`} height={`${height}`}/>
          </svg>
          <div style={{ display: 'flex', position: 'absolute', top: '0', left: '0', width: '100%', height: '100%' }}>
            <Loading color="#555" style={{ alignSelf: 'center' }} />
          </div>
        </div>
      )
    } else {
      return (
        <div style={{ width: '100%', backgroundColor: '#333', overflow: 'hidden', lineHeight: '0' }}>
          <Img src={path} />
        </div>
      )
    }
  }

}

export default ImgAsset