import React, { Component } from 'react'
import { styled } from 'glamor/styled'
import { Link } from 'react-router-dom'
import config from '../../config'

export const NoList = styled.ul`
  padding: 0;
  list-style: none;
  display: flex;
`

export const H1 = styled.h1`
  font-size: 50px;
  font-family: Eczar;
  font-weight: 700;
  line-height: 54px;
  color: #fff;
  margin: 20px 0;
  @media (max-width: 600px) {
    font-size: 40px;
    line-height: 44px;
  }
`
export const CenteredH1 = styled.h1`
  font-size: 50px;
  font-family: Eczar;
  font-weight: 700;
  line-height: 54px;
  max-width: 700px;
  color: #fff;
  margin: 20px auto;
  @media (max-width: 600px) {
    font-size: 40px;
    line-height: 44px;
  }
`
export const H3 = styled.h3`
  font-size: 30px;
  font-family: Roboto;
  font-weight: 700;
  line-height: 40px;
  color: #fff;
  margin: 20px 0;
`
export const H4 = styled.h4`
  font-size: 20px;
  font-family: Roboto;
  font-weight: 700;
  line-height: 24px;
  color: #fff;
  margin: 20px 0 10px;
`
export const P = styled.div`
  font-size: 20px;
  font-family: Roboto;
  font-weight: 100;
  line-height: 27px;
  color: #fff;
  margin: 20px 0
`
export const P2 = styled.p`
  font-size: 18px;
  font-family: Roboto;
  font-weight: 100;
  line-height: 24px;
  color: rgba(255, 255, 255, 0.7);
  margin: 10px 0
`
export const Body = styled.div`
  background: #1c5c28;
  min-height: 760px;
  padding-top: 50px;
  padding-bottom: 50px;
  display: flex;
  flow-direction: column;
`
export const Container = styled.div`
  width: 100%;
  max-width: 1024px;
  padding: 0 50px;
  margin: 0 auto;
  @media (max-width: 700px) {
    padding: 0 20px;
  }
`

export const CardH1 = styled.h4`
  font-size: 20px;
  font-family: 'Eczar';
  font-weight: 700;
  line-height: 24px;
  color: #005824;
  padding: 10px 15px 0;
`
export const CardH2 = styled.h5`
  font-size: 16px;
  font-family: Roboto;
  font-weight: 400;
  line-height: 21px;
  color: #71bf44;
  padding: 0 15px;
`
export const CardP = styled.p`
  margin-top: 5px;
  font-size: 14px;
  font-family: 'Roboto';
  font-weight: 400;
  color: #565656;
  line-height: 17px;
  padding: 0 15px 20px;
`

export const CardA = styled(Link)`
  display: flex;
  flex-direction: column;
  text-decoration: none;
  background-color: #fff;
  cursor: pointer;
  filter: saturate(100%);
  transition: filter 0.2s;
  &:hover {
    filter: saturate(0%);
  }
`

export const Center = styled.div`
  text-align: center;
`

export const Grid = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: -20px;
  @media (max-width: 850px) {
    flex-direction: column;
    align-items: center;
  }
`
export const Card = styled.div`
  flex: 1;
  max-width: 360px;
  box-shadow: 0 0 20px #134922;
  background-color: #fff;
  margin: 20px
`

export const Img = styled.img`
  width: 100%;
  height: auto;
`

export const ButtonLink = styled(Link)`
  font-family: Roboto;
  padding: 20px;
  font-size: 16px;
  text-transform: uppercase;
  font-weight: 700;
  appearance: none;
  border: none;
  border-radius: 3px;
  margin: 20px 20px 20px 0;
  outline: none;
  color: #fff;
  background-color: #6fbd44;
  &:hover {
    background-color: #fff;
    color: #6fbd44
  }
  &:active {
    color: #fff;
    background-color: #6fbd44;
  }
`

export const ButtonDiv = styled.div`
  display: inline-block;
  font-family: Roboto;
  padding: 20px;
  font-size: 16px;
  text-transform: uppercase;
  font-weight: 700;
  appearance: none;
  border: none;
  border-radius: 3px;
  margin: 20px 20px 20px 0;
  outline: none;
  color: #fff;
  background-color: #6fbd44;
  &:hover {
    background-color: #fff;
    color: #6fbd44
  }
  &:active {
    color: #fff;
    background-color: #6fbd44;
  }
`