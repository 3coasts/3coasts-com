import React, { Component } from 'react'
import { connect } from 'react-redux'
import { styled } from 'glamor/styled'
import { Link } from 'react-router-dom'
import md5 from '../../lib/md5'
import { refreshToken } from '../../actions/user'
import MediaQuery from 'react-responsive'
import Logo from './logo'
import { types } from '../../reducers'

const HeaderContainer = styled.header`
`
const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 30px;
  background-color: #fff;
`
const A = styled(Link)`
  display: flex;
  align-items: center;
  font-family: Roboto;
  font-weight: 700;
  color: #205B25;
  font-size: 16px;
  line-height: 16px;
  text-transform: uppercase;
  text-decoration: none;
  padding: 20px;
  cursor: pointer;
  background-color: #fff;
  &:hover {
    color: #fff;
    background-color: #2f2f30;
  }
`
const Nav = styled.nav`
  display: flex;
`
const Gravatar = styled.img`
  width: 50px;
  height: 50px;
  align-self: center;
  border-radius: 25px;
`
const ToggleBar = styled.div`
  background-color: #205B25;
  width: 38px;
  height: 4px;
  margin: 3px;
  border-radius: 4px;
`
const ToggleWrapper = styled.div`
  padding: 3px 20px;
  display: flex;
  flex-direction: column;
  cursor: pointer;
  &:hover {
    background-color: #2f2f30;
  }
  &:hover > div {
    background-color: #fff;
  }
  > div:first-child {
    margin-top: auto;
  }
  > div:last-child {
    margin-bottom: auto;
  }
`
@connect()
class Toggle extends Component {
  render () {
    const { dispatch } = this.props
    return (
      <ToggleWrapper onClick={() => dispatch({ type: types.menuToggle })}>
        <ToggleBar />
        <ToggleBar />
        <ToggleBar />
      </ToggleWrapper>
    )
  }
}

const StyledToggle = styled(Toggle)`
  cursor: pointer;
  &:hover {
    background-color: #fff;
  }
`

@connect(({ user }) => ({ user }))
class Links extends Component {
  onClick () {
    const { dispatch } = this.props
    dispatch({ type: types.menuToggle })
  }

  render () {
    const { user, stack } = this.props
    return (
      <Nav style={{ flexDirection: stack ? 'column' : 'row' }}>
        <A onClick={this.onClick.bind(this)} to='/about'>About</A>
        <A onClick={this.onClick.bind(this)} to='/contact'>Contact</A>
        { user.email ? (
          <A onClick={this.onClick.bind(this)} to="/user"><Gravatar src={`https://www.gravatar.com/avatar/${md5(user.email)}.jpg`} /></A>
        ) : (
          <A onClick={this.onClick.bind(this)} to='/login'>Login</A>
        ) }
      </Nav>
    )
  }
}

@connect(({ menu }) => ({ menu }))
class Header extends Component {

  componentDidMount () {
    const { dispatch } = this.props
    dispatch(refreshToken())
  }

  render () {
    const { menu } = this.props
    return (
      <HeaderContainer>
        <HeaderWrapper>
          <Logo />
          <MediaQuery maxWidth={600}>
            <StyledToggle />
          </MediaQuery>
          <MediaQuery minWidth={601}>
            <Links />
          </MediaQuery>
        </HeaderWrapper>
        <MediaQuery maxWidth={600}>
          { menu ? <Links stack /> : null }
        </MediaQuery>
      </HeaderContainer>
    )
  }
}

export default Header