import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, H1, Grid, Card, CardA, CardH1, CardH2, CardP, ButtonDiv } from '../../'
import ImgAsset from '../../image/asset'
import { fetch, create } from '../../../actions/projects'

class ProjectIndex extends Component {

  constructor () {
    super()
    this.state = { redirect: false }
  }

  componentDidMount () {
    const { dispatch } = this.props
    dispatch(fetch())
  }

  create () {
    const { dispatch } = this.props
    const title = window.prompt('Title')
    if (title.length) {
      dispatch(create({ title }))
    }
  }

  render () {
    const { projects } = this.props
    const { redirect } = this.state
    if (!projects) return null
    return (
      <Container>
        <H1>Projects</H1>
        <Grid>
          { projects.map(({ id, title, image, subtitle, teaser }) => (
            <Card key={id}>
              <CardA to={`/admin/projects/${id}`}>
                <ImgAsset image={image} width="360" height="200" />
                <CardH1>{title}</CardH1>
                <CardH2>{subtitle}</CardH2>
                <CardP>{teaser}</CardP>
              </CardA>
            </Card>
          )) }
        </Grid>
        <ButtonDiv onClick={() => this.create()}>Create a Project</ButtonDiv>
      </Container>
    )
  }

}

export default connect(({ projects }) => ({ projects }))(ProjectIndex)
