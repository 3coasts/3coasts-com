// s3://ce078c28-0bb0-4dc6-afc1-230f3795759b/body
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import ImgAsset from '../../image/asset'
import { fetch, update, remove } from '../../../actions/projects'
import { TitleInput, DraftEditor } from '../../live-edit-elements'
import ImageUpload from '../../image/upload'
import { Container, ButtonDiv, H3 } from '../../'
import { SecondaryAction } from '../../form-elements'
import { types } from '../../../reducers'
import { styled } from 'glamor/styled'
import { Editor } from '../../editor'

const TeaserInput = styled.textarea`
  width: 100%;
  margin-top: 5px;
  font-size: 20px;
  font-family: Roboto;
  font-weight: 100;
  line-height: 27px;
  color: #fff;
  padding: 0;
  background-color: transparent;
  border: none;
  appearance: none;
  display: block;
  outline: none;
  color: #fff;
`

@connect(({ projects }, { match: { params: { id } } }) => ({ project: projects.find(x => x.id === id) }))
export default class ProjectEdit extends Component {

  constructor () {
    super()
    this.state = { saved: false }
  }

  componentDidMount () {
    const { dispatch, project: { id } } = this.props
    dispatch(fetch(id))
  }

  componentDidUpdate (prevProps) {
    const { project: { isDirty = false } } = this.props
    if (prevProps.project.isDirty && !isDirty) {
      this.setState({ saved: true })
    }
  }

  save () {
    const { dispatch, project } = this.props
    dispatch(update(project))
  }

  update (patch) {
    const { dispatch, project } = this.props
    dispatch({ type: types.projects.edit, project: { ...project, ...patch } })
  }

  destroy () {
    const { dispatch, project: { id } } = this.props
    dispatch(remove(id))
  }

  render () {
    const { project = {} } = this.props
    const { saved } = this.state
    return (
      <Container>
        { !project.id || saved ? <Redirect to='./' /> : null }
        <TitleInput
          value={project.title}
          type="text"
          onChange={(e) => this.update({ title: e.target.value })} />
        <H3>Teaser Image</H3>
        <ImageUpload image={project.image} onChange={image => this.update({ image })}><br /><ButtonDiv>Upload</ButtonDiv></ImageUpload>
        <H3>Teaser</H3>
        <TeaserInput onChange={(e) => this.update({ teaser: e.target.value })} value={project.teaser} />
        <H3 style={{ marginBottom: 0 }}>Body Content</H3>
        <Editor components={project.body} onChange={body => this.update({ body })} />
        <ButtonDiv onClick={() => this.save()}>Save</ButtonDiv>
        <ButtonDiv onClick={() => this.destroy()}>Delete</ButtonDiv>
      </Container>
    )
  }

}
