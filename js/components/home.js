import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  CenteredH1, H3, P, Container, Grid, Card, CardH1, CardH2, CardP, CardA, Center
} from './'
import config from '../../config'
import { Register, Login, ConfirmRegistration } from './auth'
import axios from 'axios'
import { fetch } from '../actions/projects'
import ImgAsset from './image/asset'
import Loading from './loading'

class Home extends Component {

  componentDidMount () {
    const { dispatch } = this.props
    dispatch(fetch())
  }

  render () {
    const { projects } = this.props
    return (
      <Container className="home-component">
        <Center>
          <CenteredH1>We build digital web applications for the internet.</CenteredH1>
          <P>3Coasts is a web development and design company in the heart of New Orleans.  Our team designs and
            develops beautiful, state-of-the-art web applications. Our clients are diverse, but there's a common
            denominator they share: they need a web team who can not only run with them, but keep one step ahead.</P>
          <H3>Featured Projects</H3>
        </Center>
        <Grid>
          { projects.map(project => (
            <Card key={project.id}>
              <CardA to={`/projects/${project.id}`}>
                <ImgAsset image={project.image} gravity="North" width="700" height="380" />
                <CardH1>{ project.title }</CardH1>
                <CardH2>{ project.subtitle }</CardH2>
                <CardP>{ project.teaser }</CardP>
              </CardA>
            </Card>
          )) }
        </Grid>
      </Container>
    )
  }

}

export default connect(({ projects }) => ({ projects }))(Home)