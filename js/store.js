import { createStore, applyMiddleware, combineReducers } from 'redux'
import { routerMiddleware, routerReducer as routing } from 'react-router-redux'
import createLogger from 'redux-logger'
import thunk from 'redux-thunk'
import reducers from './reducers'

const isBrowser = new Function("try {return this===window;}catch(e){ return false;}")()

const initialState = isBrowser ? window.__INITIAL_STATE__ : {}
if (isBrowser) {
  delete window.__INITIAL_STATE__
}

export const configureStore = (initialState) => {
  const middlewares = [thunk, createLogger()]
  return createStore(reducers, initialState || {}, applyMiddleware(...middlewares))
}

const store = configureStore(initialState)

if (isBrowser) {
  if (window.location.hostname === 'local.host') {
    window.store = store
  }
}

export default store