import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './app'
import config from '../config'
import { Provider } from 'react-redux'
import store from './store'
import { rehydrate } from 'glamor'

if (window.location.host === 'localhost:3000') {
  window.location.assign('http://local.host:3000')
}

rehydrate(window._glam)

render(
  <Provider store={store}>
    <BrowserRouter basename={config.basePath}>
      <App />
    </BrowserRouter>
  </Provider>
, document.getElementById('app-container'))
