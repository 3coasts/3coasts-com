import axios from 'axios'
import config from '../../config'
import { types } from '../reducers'

export const fetch = () => dispatch => {
  axios({
    method: 'GET',
    url: `${config.basePath}/api/projects`
  }).then(({ data: projects }) => {
    dispatch({ type: types.projects.fetched, projects })
  })
}

export const update = ({ isDirty, ...project }) => dispatch => {
  axios({
    method: 'POST',
    url: `${config.basePath}/api/projects`,
    data: project
  }).then(({ data: project }) => {
    dispatch({ type: types.projects.updated, project })
  })
}

export const create = ({ isDirty, ...project }) => dispatch => {
  axios({
    method: 'POST',
    url: `${config.basePath}/api/projects`,
    data: project
  }).then(({ data: project }) => {
    dispatch({ type: types.projects.added, project })
  })
}

export const remove = id => dispatch => {
  axios({
    method: 'DELETE',
    url: `${config.basePath}/api/projects/${id}`
  }).then(({ data: project }) => {
    dispatch({ type: types.projects.removed, id })
  })
}