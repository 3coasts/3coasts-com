import axios from 'axios'
import config from '../../config'
import { types } from '../reducers'
import {
  Config,
  CognitoIdentityCredentials,
  CognitoUserPool,
  CognitoUserAttribute,
  AuthenticationDetails,
  CognitoUser
} from "amazon-cognito-identity-js"

const userPool = new CognitoUserPool({
  UserPoolId: config.aws.userPoolId,
  ClientId: config.aws.clientId
})

const sendIdToken = result => dispatch => {
  axios({
    url: `${config.basePath}/api/auth`,
    method: 'GET',
    headers: { Authorization: result.getIdToken().jwtToken }
  }).then(({ data: user }) => {
    dispatch({ type: types.user.auth, user })
  }).catch(err => console.error(err))
}

export const refreshToken = () => dispatch => {
  var cognitoUser = userPool.getCurrentUser()
  if (cognitoUser != null) {
    cognitoUser.getSession((err, session) => {
      if (err) {
        console.error(err)
      } else {
        dispatch(sendIdToken(session))
      }
    })
  }
}

export const confirmRegistration = (Username, code) => dispatch => {
  var userData = { Username, Pool : userPool }
  var cognitoUser = new CognitoUser(userData)
  cognitoUser.confirmRegistration(code, true, (err, result) => {
    if (err) {
      alert(err)
      return
    }
    dispatch({ type: types.user.patch, user: { confirmed: true } })
  })
}

export const forgotPassword = Username => dispatch => {
  var cognitoUser = new CognitoUser({ Username, Pool: userPool })
  cognitoUser.forgotPassword({
    onSuccess: () => dispatch({
      type: types.user.auth,
      user: { email: Username, reset: true }
    }),
    onFailure: err => alert(err)
  })
}

export const login = (Username, Password) => dispatch => {
  var authenticationDetails = new AuthenticationDetails({ Username, Password })
  var cognitoUser = new CognitoUser({ Username, Pool: userPool })
  cognitoUser.authenticateUser(authenticationDetails, {
    onSuccess: (s) => dispatch(sendIdToken(s)),
    onFailure: (e) => console.error(e)
  })
}

export const register = (email, password) => dispatch => {
  const attributeList = [new CognitoUserAttribute({ Name: 'email', Value: email })]
  userPool.signUp(email, password, attributeList, null, (err, result) => {
    if (err) {
      console.log(err)
      return
    }
    dispatch({ type: types.user.auth, user: { email, password, registered: true } })
  })
}

export const resendConfirmation = Username => dispatch => {
  var cognitoUser = new CognitoUser({ Username, Pool: userPool })
  cognitoUser.resendConfirmationCode(function(err, result) {
    if (err) {
      alert(err)
      return
    }
    dispatch({ type: types.user.auth, user: { email: Username, resent: true } })
  })
}

export const resetPassword = (Username, Password, code) => dispatch => {
  var cognitoUser = new CognitoUser({ Username, Pool: userPool })
  const authDetails = new AuthenticationDetails({ Username, Password })
  cognitoUser.confirmPassword(code, Password, {
    onSuccess: () => dispatch(login(Username, Password)),
    onFailure: (err) => alert(err)
  })
}

export const signOut = email => dispatch => {
  const cognitoUser = new CognitoUser({ Username: email, Pool: userPool })
  axios({
    method: 'DELETE',
    url: `${config.basePath}/api/auth`
  }).then(() => {
    cognitoUser.signOut()
    dispatch({ type: types.user.auth, user: null })
  })
}
