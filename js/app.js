import React from 'react'
import { Route } from 'react-router-dom'
import { Body } from './components'
import Footer from './components/footer'

import Home from './components/home'
import About from './components/about'
import Contact from './components/contact'
import Project from './components/project'
import Login from './components/auth/login'
import Register from './components/auth/register'
import ForgotPassword from './components/auth/forgot-password'
import ResetPassword from './components/auth/reset-password'
import User from './components/user'
import ProjectIndex from './components/admin/projects'
import ProjectEdit from './components/admin/projects/edit'
import ConfirmRegistration from './components/auth/confirm-registration'
import Header from './components/header'

export default () => {
  return (
    <div>
      <Header />
      <Body>
        <Route exact path="/" component={Home} />
        <Route exact path="/about" component={About} />
        <Route exact path="/projects/:id" component={Project} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/signup" component={Register} />
        <Route exact path="/confirm-registration" component={ConfirmRegistration} />
        <Route exact path="/forgot-password" component={ForgotPassword} />
        <Route exact path="/reset-password" component={ResetPassword} />
        <Route exact path="/user" component={User} />
        <Route exact path="/admin/projects" component={ProjectIndex} />
        <Route exact path="/admin/projects/:id" component={ProjectEdit} />
      </Body>
      <Footer />
    </div>
  )
}